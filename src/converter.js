/**
 * @param {string} hex
 * @param {string} rgb
 * @returns {string}
 * */

const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}


module.exports = {
    /** 
     * @param {number} red
     * @param {number} green
     * @param {number} blue
     * */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); //"2"
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    },

    hexToRgb: (hex) => {
        let rgbValues = [parseInt(hex[0], 16),parseInt(hex[1], 16),parseInt(hex[2], 16)]; 
        return rgbValues;
    }
}